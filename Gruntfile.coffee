module.exports = (grunt) ->

  grunt.loadNpmTasks 'grunt-nodemon'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-concurrent'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'
  grunt.loadNpmTasks 'grunt-contrib-uglify'

  grunt.initConfig

    clean: ['public/target']

    cssmin: 
      target:
        src: ['public/src/normalize.css', 'public/src/style.css']
        dest: 'public/target/app.css'

    uglify:
      target:
        src: [
          'public/src/angular.min.js'
          'public/src/ui-event.js'
          'public/src/ui-map.js'
          'public/src/app.js'
        ]
        dest: 'public/target/app.js'

    nodemon:
      run:
        options:
          file: 'server.js'
          watchedExtensions: ['js', 'html']
          ignoredFiles: [
            'public/*'
            'test/*'
            'data/*'
            '.git/*'
          ]

    watch: 
      livereload:
        options: 
          livereload: true
        files: [
          "public/target/*.*"          
        ]

    concurrent:
      main:
        tasks: ['nodemon', 'watch']
        options:
          logConcurrentOutput: true

  grunt.registerTask 'default', ['clean', 'cssmin', 'uglify']
  grunt.registerTask 'dev', ['default', 'concurrent']

