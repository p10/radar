var http = require('http');
var TIMEOUT = 5000;

var fetch = function(dateString, done) {
  var url = 'http://www.torun.pl/sites/radar/dane.php?id=' + dateString;
  var body = '';
  var req = http.get(url, function(res) {
    res.on('data', function(chunk) {
      body += chunk;
    });
    res.on('end', function() {
      done(null, body);
    });
  });
	req.on('socket', function(socket) {
		socket.setTimeout(TIMEOUT);
		socket.on('timeout', function() {
			req.abort();
			done(new Error('Timeout while accessing torun.pl'));
		});
	})
	req.on('error', function(e) {
    done(e);
  });  
};

var format = function(data) {
  var i = 0, cords, name, out = [];
  data = data.split(';').filter(function(line) {
    return line.trim().length > 0;
  });
  data.shift();
  for ( ; i < data.length; i += 2) {
    cords = data[i].match(/(\d+\.\d+)/g);
    name = data[i + 1].match(/'Lokalizacja: (.+) Data.*'/)[1];
    out.push({
      name: name,
      lat: cords[0],
      lng: cords[1]
    });
  }
  return out;
};

var zeroize = function(x) {
  x = ''+x;
  return (x.length === 1) ? '0'+x : x;
};

module.exports = function(date, done) {
  var dateString = [
    date.getFullYear(), 
    zeroize(date.getMonth() + 1), 
    zeroize(date.getDate())
  ].join('-');
  fetch(dateString, function(e, data) {
    if (e) return done(e);
		try {
			data = format(data);
		} catch (e) {
			return done(new Error('Error while formating response from torun.pl'))
		}
    return done(null, data);
  });
};
