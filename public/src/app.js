angular.module('app', ['ui.map']).controller('AppCtrl', ['$scope', '$http', function($scope, $http) {
  $scope.markers = [];
  $scope.mapOptions = {
    center: new google.maps.LatLng(53.0213441, 18.5933948),
    zoom: 11,
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var addMarker = function(lat, lng) {
    var point = new google.maps.LatLng(lat, lng);
    $scope.markers.push(new google.maps.Marker({
      map: $scope.map,
      position: point
    }));
  };
  $http.get('/today.json').success(function(data) {
    $scope.radars = data;
    angular.forEach(data, function(radar) {
      addMarker(radar.lat, radar.lng);
    });
  }).error(function(data) {
		console.error('error', data.error);
		alert(data.error);
	});
}]);