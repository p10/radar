var express = require('express');
var radar = require('./');
var http = require('http');
var port = process.env.PORT || 3000;
var app = express();

app.use(express.logger('dev'));
app.use(express.static('public'));
app.use(express.errorHandler());
app.use(app.router);

app.use(function(err, req, res, next) {
	console.log('SERVER ERROR:', err);
	res.status(500).json({error: err.message});
});

app.get('/today.json', function(req, res, next) {
	radar(new Date(), function(e, data) {
    if (e) return next(e);
    res.json(data, 500);
  });
});

http.createServer(app).listen(port, function() {
  console.log('Express server listening on port '+ port +' in '+ app.settings.env +' mode');
});